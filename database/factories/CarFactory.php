<?php

use Faker\Generator as Faker;

$factory->define(App\Car::class, function (Faker $faker) {
    return [
        'brand' => $faker->word,
        'color' => $faker->colorName,
        'model' => $faker->word,
        'year' => $faker->year,
        'type_id' => $faker->numberBetween($min = 1, $max = 3)
    ];
});
