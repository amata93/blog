<?php

use Illuminate\Database\Seeder;

class CarsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->delete();
        //insert some dummy records
        DB::table('cars')->insert(array(
            array('brand'=>'Kia',"color"=>"white", 'year' => '2003','type_id'=>3,'model'=>'Sportage'),
            array('brand'=>'Suzuki',"color"=>"grey", 'year' => '2016','type_id'=>2,'model'=>'Ciaz'),
            array('brand'=>'Nissan',"color"=>"black", 'year' => '2012','type_id'=>1,'model'=>'350z')
        ));
        factory(App\Car::class, 50)->create();
    }
}
