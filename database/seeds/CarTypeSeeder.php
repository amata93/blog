<?php

use Illuminate\Database\Seeder;

class CarTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_types')->delete();
        //insert some dummy records
        DB::table('car_types')->insert(array(
            array('type'=>'Race'),
            array('type'=>'Sedan'),
            array('type'=>'4x4')

        ));
    }
}
