<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CarsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function migrationTest()
    {
        $this->assertDatabaseHas('users', [
            'email' => 'andres.mata@softonitg.com'
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCarTypeRelation()
    {
        $car = factory(\App\Car::class)->make();
        $this->assertDatabaseHas('car_types', [
            'id' => $car->type_id
        ]);

        $car->type_id=90000;
        $this->assertDatabaseMissing('car_types', [
            'id' => $car->type_id
        ]);
    }


}
