<?php

namespace App\Http\Controllers\Cars;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Car;
use App\Http\Resources\CarResource;
use App\Http\Requests\StoreCar;
use App\Http\Requests\UpdateCar;
use Response;


class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = CarResource::collection(Car::all());
        return Response::json($cars, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCar $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCar $request)
    {
        $car = new Car;
        $car->brand = $request->brand;
        $car->color = $request->color;
        $car->model = $request->model;
        $car->year = $request->year;
        $car->type_id = $request->type;
        $car->save();
        return Response(new CarResource($car));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        if ($car != null) {
            return Response(new CarResource($car));
        }
        return response('Not Found', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCar $request
     * @param  \App\Car $car
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCar $request, Car $car)
    {
        if ($car != null) {
            $car->brand = $request->get('brand', $car->brand);
            $car->color = $request->get('color', $car->color);
            $car->model = $request->get('model', $car->model);
            $car->year = $request->get('year', $car->year);
            $car->type_id = $request->get('type_id', $car->type_id);
            $car->save();
            return Response(new CarResource($car));
        }
        return response('Not Found', 404);
    }

    /**
     * Remove the specified resource from storage.
     * @throws
     * @param  \App\Car $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        if ($car != null) {
            $car->delete();
        }
        return $this->index();
    }
}
