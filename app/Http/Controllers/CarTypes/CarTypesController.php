<?php

namespace App\Http\Controllers\CarTypes;

use Response;
use App\CarType;
use App\Http\Requests\StoreCarType;
use App\Http\Requests\UpdateCarType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carTypes = CarType::all();
        return Response::json($carTypes, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCarType $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCarType $request)
    {
        $carType = new CarType;
        $carType->type = $request->type;
        $carType->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarType $carType
     * @return \Illuminate\Http\Response
     */
    public function show(CarType $carType)
    {
        return $carType->cars;
        if ($carType != null) {
            return Response($carType);
        }
        return Response('Not Found', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCarType $request
     * @param  \App\CarType $carType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarType $request, CarType $carType)
    {
        if ($carType != null) {
            $carType->type = $request->get('type', $carType->type);
            $carType->save();
            return Response(    $carType);
        }
        return response('Not Found', 404);
    }

    /**
     * Remove the specified resource from storage.
     * @throws
     * @param  \App\CarType $carType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarType $carType)
    {
        if ($carType != null) {
            $carType->delete();
        }
        return $this->index();
    }
}
