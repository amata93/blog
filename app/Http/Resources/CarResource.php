<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CarResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'brand' => $this->brand,
            'color' => $this->color,
            'model' => $this->model,
            'year' => $this->year,
            'type' => $this->carType->type
        ];
    }
}
