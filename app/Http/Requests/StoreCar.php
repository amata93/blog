<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'required',
            'color' => 'required',
            'model' => 'required',
            'year' => 'required',
            'type' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'brand.required' => 'A Brand is required',
            'color.required' => 'A color is required',
            'model.required' => 'A model is required',
            'year.required' => 'A year is required',
            'type.required' => 'A type is required'
        ];
    }
}
