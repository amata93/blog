<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    protected $table = 'car_types';

    protected $hidden = ['created_at','updated_at'];

    public $fillable = [
        'type'
    ];

    public function cars(){
        return $this->hasMany('App\Car','type_id','id');
    }

}
