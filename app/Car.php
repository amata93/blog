<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table = 'cars';

    protected $hidden = ['created_at','updated_at'];

    public $fillable = [
        'brand',
        'color',
        'model',
        'year',
        'type_id'
    ];

    public function carType(){
        return $this->belongsTo('App\CarType','type_id','id');
    }
}
